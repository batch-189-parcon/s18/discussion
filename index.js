// function parameters are variables that wait for a value from an arguement in the function invocation
 function sayMyName(name){
	console.log('My name is  ' + name)
}

// function arguements are values like strings, numbers, etc. that you can pass onto the function you are invoking
sayMyName('Slim Shady')

// you can re-use functions by invoking them at any time or any part of your code, Make sure you have declared them first
sayMyName('Vice Ganda')
sayMyName('Joferlyn')


// you can also pass variables as arguements to a function
let myName = 'Earl'
sayMyName(myName)


function sumOfTwoNumbers(firstNumber, secondNumber) {
	let sum = 0

	sum = firstNumber + secondNumber

	console.log(sum)
}

sumOfTwoNumbers(10, 15)
sumOfTwoNumbers(99, 1)
sumOfTwoNumbers(50, 20)


// you can pass a function as an argument for another function. do not pass that function with parenthesis, only with its function name 
function argumentFunction() {
	console.log('This is a function that was passed as an argument')
}

function parameterFunction(argumentFunction){
	argumentFunction()
}

parameterFunction(argumentFunction)



// REAL WORLD APPLICATION
/*
	imagine a product page where you have a 'add to card' button wherein you have the button element itself displayed on the page BUT you don't have the functionality of it yet. This is where passing a function as an argument comes in, in order for you to add or have access to a function to add functionality to your button once it is clicked.
*/
function addProductToCart(){
	console.log('Click me to add product to cart')
}

let addToCartBtn = document.querySelector('#add-to-card-btn')

addToCartBtn.addEventListener('click', addProductToCart)


// if you add extra or  if you lack an argument, js wont throw an error at you, instead it will ignore the extra argument and turn the lacking parameter into un undefined
function displayFullName( firstName, middleName, lastName, extraText) {
	console.log('Your full name is ' + firstName + " " + middleName + " " + lastName + " " + extraText)
}


displayFullName('Amber', 'A', 'Baad', 'Yeah')



// STRING INTERPOLATION - insted of regular concatination, we can interpolate of inject the variables within the string itself
function displayMovieDetails(title, synopsis, director) {
	console.log(`The movie is titled ${title}`)
	console.log(`The synopsis is ${synopsis}`)
	console.log(`The director is ${director}`)
}

displayMovieDetails('Bang', 'pew pew pew', 'Ronaldo Gunzalles')

//You can use double quotation OR backticks to accomodate for strings that have apostrophes
// console.log('Congrats')
// console.log("Don't kill")

function displayPlayerDetails(name, age, playerClass){
	// console.log(`Player name: ${name}`)
	// console.log(`Player age: ${name}`)
	// console.log(`Player class: ${playerClass}`)
	let playerDetails = `Name: ${name}, Age: ${age}, Class ${playerClass}`

	return playerDetails

	console.log('Hello!')
}

console.log (displayPlayerDetails('Elsworth', 120, 'Paladin'))	